# Micro:Bit Watcher
Matthew C. Jadud

When you are programming on [makecode.microbit.org](https://makecode.microbit.org/), you may find yourself constantly hitting "Download." Then, you have to copy the .hex file that you download over to your Micro:Bit. This annoyed me.

Micro:Bit Watcher keeps an eye on your downloads folder. Whenever a new .hex file appears, the Watcher copies that .hex file over to your Micro:Bit. And, if you like, it can also clean up .hex files after being copied.

# Getting the Software

Micro:Bit Watcher should run on Mac, Windows, and Linux.

* Download for Mac.
* Download for Windows.
* Download for Linux.

# Problems? Requests?

If you have problems or requests, please [drop me a note](https://bitbucket.org/jadudm/microbit-watcher/issues?status=new&status=open) using the BitBucket issue tracker.

# Contributing

If you want to contribute to Micro:Bit Watcher, you'll need to install the Racket programming language for your favorite operating system. This is easy-peasy; [Dr. Racket, the native IDE for this Scheme-like language](http://racket-lang.org/), Just Works everywhere.


